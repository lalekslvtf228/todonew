// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces

/// <reference types="@sveltejs/kit" />
interface User {
	name: string,
	token: string
}

interface SessionData {
	views: number,
	data1: number[],
	data2: string[],
	user: User
}
interface Country {
	id: number,
	name: string
}

declare global {
	namespace App {
		interface Locals {
			session: import('svelte-kit-cookie-session').Session<SessionData>,

		}

		interface Session extends SessionData { }
		// interface Error {}

		interface Locals {
			stop: boolean
		}

		interface PageData { session: SessionData; }
		// interface Platform {}
	}
}

export { };
