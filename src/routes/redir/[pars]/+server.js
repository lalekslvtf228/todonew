import { redirect } from '@sveltejs/kit'

/** @type {import("./$types").RequestHandler} */
export const GET = async ({ params }) => {
    console.log('GET', params)
    const { pars } = params
    // throw redirect(301, `/todo/${pars}`)
    throw redirect(301, '/about')
}