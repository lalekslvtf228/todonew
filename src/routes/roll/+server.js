import { json } from '@sveltejs/kit';

/**   @type {import('./$types').RequestHandler} */
export function GET() {
    const number = Math.floor(Math.random() * 10) + 100;
    let num = { "nm": number }
    //Вариант 1
    return new Response(JSON.stringify(num), {
        headers: {
            'Content-Type': 'application/json'
        }
    });
    //Вариант 2
    // return json(num)
}